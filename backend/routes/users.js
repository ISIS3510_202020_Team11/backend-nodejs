var express = require('express')
var router = express.Router()
const admin = require('firebase-admin')

//Save the information of a user using his UID
router.post('/save-info', (req, res, next) => {
  if (res.locals.token) {
    let token = res.locals.token

    admin
      .auth()
      .getUser(token.uid)
      .then((result) => {
        var db = admin.database()
        var ref = db.ref('Users')
        const uid = result.uid
        const userInfo = req.body

        ref
          .child(uid)
          .set({
            name: userInfo.name,
            last_name: userInfo.lastName,
            birthdate: userInfo.birthdate,
            hobbies: userInfo.hobbies,
          })
          .then(() => {
            res.send({ response: 'User information saved!' })
          })
          .catch((error) => {
            res.send(error)
          })
      })
      .catch(() => {
        res.status(404).send('No user found with this uid')
      })
  } else {
    res.status(500).send('res.locals.token was not found')
  }
})

//Save the swiped right hobbies of a user using his UID
router.put('/swipe', (req, res, next) => {
  if (res.locals.token) {
    let token = res.locals.token
    admin
      .auth()
      .getUser(token.uid)
      .then((result) => {
        var db = admin.database()
        var ref = db.ref('Users')
        const uid = result.uid
        const userInfo = req.body

        ref
          .child(uid)
          .update({
            Hobbies: userInfo.hobbies,
            HobbiesNot: userInfo.hobbiesNot,
          })
          .then(() => {
            res.send({ response: 'User hobbies saved!' })
          })
          .catch((error) => {
            res.send(error)
          })
      })
      .catch(() => {
        res.status(404).send('No user found with this uid')
      })
  } else {
    res.status(500).send('res.locals.token was not found')
  }
})

//Save interactions of a user using his UID
router.put('/interactions', (req, res, next) => {
  if (res.locals.token) {
    let token = res.locals.token
    admin
      .auth()
      .getUser(token.uid)
      .then((result) => {
        var db = admin.database()
        var ref = db.ref('Users')
        const uid = result.uid
        const userInfo = req.body

        let time = process.hrtime()

        ref
          .child(uid)
          .child('Interactions')
          .child(time[1])
          .set(userInfo.interactions)
          .then(() => {
            res.send({ response: 'User interactions saved!' })
          })
          .catch((error) => {
            res.send(error)
          })
      })
      .catch(() => {
        res.status(404).send('No user found with this uid')
      })
  } else {
    res.status(500).send('res.locals.token was not found')
  }
})

//Save the swiped right hobbies of a user using his UID
router.put('/swipe', (req, res, next) => {
  if (res.locals.token) {
    let token = res.locals.token
    admin
      .auth()
      .getUser(token.uid)
      .then((result) => {
        var db = admin.database()
        var ref = db.ref('Users')
        const uid = result.uid
        const userInfo = req.body

        ref
          .child(uid)
          .push({
            Hobbies: userInfo.hobbies,
            HobbiesNot: userInfo.hobbiesNot,
          })
          .then(() => {
            res.send({ response: 'User hobbies saved!' })
          })
          .catch((error) => {
            res.send(error)
          })
      })
      .catch(() => {
        res.status(404).send('No user found with this uid')
      })
  } else {
    res.status(500).send('res.locals.token was not found')
  }
})

module.exports = router
