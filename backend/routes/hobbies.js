var express = require('express')
var router = express.Router()
const admin = require('firebase-admin')

router.get('/', (req, res, next) => {
  let db = admin.database()
  let ref = db.ref('Hobbies')

  ref.once('value', (snapshot) => {
    res.send(snapshot.val())
  })
})

module.exports = router
