var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var admin = require('firebase-admin')
var serviceAccount = require('./config/java-projec-firebase-adminsdk-enccm-ea7b092847.json')

var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var hobbiesRouter = require('./routes/hobbies')
var activitiesRouter = require('./routes/activities')

var app = express()

//Firebase setup
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://java-projec.firebaseio.com',
})

/**
 * Checks if the user is authenticated
 */
function checkAuth(req, res, next) {
  if (req.headers.authtoken) {
    admin
      .auth()
      .verifyIdToken(req.headers.authtoken)
      .then((decodedToken) => {
        res.locals.token = decodedToken
        next()
      })
      .catch(() => {
        res.status(403).send('Unauthorized')
      })
  } else {
    res.status(403).send('Unauthorized')
  }
}

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', checkAuth)
app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/hobbies', hobbiesRouter)
app.use('/activities', activitiesRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
